/******************************************************/
/* Author:          Polydouri Andrianna (M1598)       */
/* Class:           Advanced Artificial Intelligence  */
/* Exercise:        N-Fractions Problem in SWI Prolog */
/* Code Solution:   Version 02                        */
/* Date:            January 2019                      */
/******************************************************/

/* Constraint Logic Programming over Finite Domains library */
?- use_module(library(clpfd)).

/*======================================================*/
/*                 running commands                     */
/*======================================================*/
go6 :- time(six_fractions).
go5 :- time(five_fractions).
go4 :- time(four_fractions).
go3 :- time(three_fractions).
go2 :- time(two_fractions).
/*======================================================*/

/* X appears exactly N times in list */
exactly(_, [], 0).
exactly(X, [Y|L], N) :-
    X #= Y #<==> B,
    N #= M+B,
    exactly(X, L, M).
/*-----------------------------------*/

six_fractions :-
        six_fractions_impl(Digits),
        writeln(Digits).

six_fractions_impl(Digits) :-  

        /* Digits: the solution list */        
        Digits=[A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R],
        Digits ins 1..9,

        Denominators = [D1,D2,D3,D4,D5,D6],
        Denominators ins 11..99,

        /* Each digit from 1 to 9 to be used exactly two times in Digits */
        global_cardinality(Digits, [1-2,2-2,3-2,4-2,5-2,6-2,7-2,8-2,9-2]),

        D1 #= (B*10)+C,
        D2 #= (E*10)+F,
        D3 #= (H*10)+I,
        D4 #= (K*10)+L,
        D5 #= (N*10)+O,
        D6 #= (Q*10)+R,

        /* fractions add to 1 */
        A*D2*D3*D4*D5*D6 + D*D1*D3*D4*D5*D6 + G*D1*D2*D4*D5*D6 + J*D1*D2*D3*D5*D6 + M*D1*D2*D3*D4*D6 + P*D1*D2*D3*D4*D5 #= D1*D2*D3*D4*D5*D6,

        /* mo mirror solutions */
        A*D2 #=< D*D1,
        D*D3 #=< G*D2,
        G*D4 #=< J*D3,
        J*D5 #=< M*D4,
        M*D6 #=< P*D5,

        /* search */
        labeling([ff,bisect],Digits).

five_fractions :-
        five_fractions_impl(Digits),
        writeln(Digits).

five_fractions_impl(Digits) :-  
        
        /* Digits: the solution list */        
        Digits=[A,B,C,D,E,F,G,H,I,J,K,L,M,N,O],
        Digits ins 1..9,

        Denominators = [D1,D2,D3,D4,D5],
        Denominators ins 11..99,

        /* CounterList: counter of each digit (1..9) appearing in the solution list */
        CounterList = [X1,X2,X3,X4,X5,X6,X7,X8,X9],
        CounterList ins 1..2,
        /* three out of nine digits should appear exactly once in Digits */
        exactly(1,CounterList,3),
        
        global_cardinality(Digits,[1-X1,2-X2,3-X3,4-X4,5-X5,6-X6,7-X7,8-X8,9-X9]),

        D1 #= (B*10)+C,
        D2 #= (E*10)+F,
        D3 #= (H*10)+I,
        D4 #= (K*10)+L,
        D5 #= (N*10)+O,

        /* fractions add to 1 */
        A*D2*D3*D4*D5 + D*D1*D3*D4*D5 + G*D1*D2*D4*D5 + J*D1*D2*D3*D5 + M*D1*D2*D3*D4 #= D1*D2*D3*D4*D5,

        /* mo mirror solutions */
        A*D2 #=< D*D1,
        D*D3 #=< G*D2,
        G*D4 #=< J*D3,
        J*D5 #=< M*D4,

        /* search */
        labeling([ff,bisect],Digits).

four_fractions :-
        four_fractions_impl(Digits),
        writeln(Digits).
        
four_fractions_impl(Digits) :-  
        
        /* Digits: the solution list */        
        Digits = [A,B,C,D,E,F,G,H,I,J,K,L],
        Digits ins 1..9,

        Denominators = [D1,D2,D3,D4],
        Denominators ins 11..99,

        /* CounterList: counter of each digit (1..9) appearing in the solution list */
        CounterList = [X1,X2,X3,X4,X5,X6,X7,X8,X9],
        CounterList ins 1..2,
        /* three out of nine digits should appear exactly 2 times in Digits */
        exactly(2,CounterList,3),
        
        global_cardinality(Digits,[1-X1,2-X2,3-X3,4-X4,5-X5,6-X6,7-X7,8-X8,9-X9]),

        D1 #= (B*10)+C,
        D2 #= (E*10)+F,
        D3 #= (H*10)+I,
        D4 #= (K*10)+L,

        /* fractions add to 1 */
        A*D2*D3*D4 + D*D1*D3*D4 + G*D1*D2*D4 + J*D1*D2*D3 #= D1*D2*D3*D4,

        /* mo mirror solutions */
        A*D2 #=< D*D1,
        D*D3 #=< G*D2,
        G*D4 #=< J*D3,

        /* search */
        labeling([ff,bisect],Digits).

three_fractions :-
        three_fractions_impl(Digits),
        writeln(Digits).
        

three_fractions_impl(Digits) :-  
        
        /* Digits: the solution list */        
        Digits=[A,B,C,D,E,F,G,H,I],
        Digits ins 1..9,

        all_different(Digits),

        D1 #= (B*10)+C,
        D2 #= (E*10)+F,
        D3 #= (H*10)+I,

        /* fractions add to 1 */
        A*D2*D3 + D*D1*D3 + G*D1*D2 #= D1*D2*D3,

        /* mo mirror solutions */
        A*D2 #=< D*D1,
        D*D3 #=< G*D2,

        /* search */
        labeling([ff],Digits).

two_fractions :-
        two_fractions_impl(Digits),
        writeln(Digits).
        

two_fractions_impl(Digits) :-  
        
        /* Digits: the solution list */        
        Digits=[A,B,C,D,E,F],
        Digits ins 1..9,

        Denominators = [D1,D2],
        Denominators ins 11..99,

        all_different(Digits),

        D1 #= (B*10)+C,
        D2 #= (E*10)+F,

        /* fractions add to 1 */
        A*D2 + D*D1 #= D1*D2,

        /* mo mirror solutions */
        A*D2 #=< D*D1,

        /* search */
        labeling([ff],Digits).


